# SHELL
alias clear="printf '\033c'" # faster than ncurses clear by a lot
alias c='clear'
alias q='exit'
alias bashrc="vim ~/.bashrc && source ~/.bashrc"
# GIT
alias g="git"
alias lc='fc -nl $HISTCMD'
# LS
alias ls='exa -hal --color=always --group-directories-first --icons' # my preferred listing
alias la='ls'
alias ll=ls
alias l=ls
# GREP
alias grep='grep --color=auto'
alias river='/home/aryak/startriver'
# Readability
alias cp="cp -iv"
alias mv='mv -iv'
alias rm='rm -iv'
alias df='df -h'
alias free='free -m'
alias mkdir='mkdir -pv'
# youtube-dl
alias yta-aac="yt-dlp --extract-audio --audio-format aac "
alias yta-best="yt-dlp --extract-audio --audio-format best "
alias yta-flac="yt-dlp --extract-audio --audio-format flac "
alias yta-m4a="yt-dlp --extract-audio --audio-format m4a "
alias yta-mp3="yt-dlp --extract-audio --audio-format mp3 "
alias yta-opus="yt-dlp --extract-audio --audio-format opus "
alias yta-vorbis="yt-dlp --extract-audio --audio-format vorbis "
alias yta-wav="yt-dlp --extract-audio --audio-format wav "
alias ytv-best="yt-dlp -f bestvideo+bestaudio "
# the terminal rickroll
alias rr='curl -s -L https://raw.githubusercontent.com/keroserene/rickrollrc/master/roll.sh | bash'
# SUDO
for command in mount umount su reboot shutdown xbps-install xbps-rindex xbps-remove xbps-reconfigure xbps-query xbps-pkgdb xbps-alternatives apt apt-file; do
    alias $command="sudo $command"
done; unset command
alias se="sudoedit"
# Package Management
alias ai='apt install'
alias au='apt update && apt upgrade'
alias ar='apt remove'
alias as='apt search'
alias af='apt-file search'
# Navigation
alias ..='cd ..'
alias ...='cd ../../'
alias ....='cd ../../../'
alias .....='cd ../../../../'
# Vim
alias vim=nvim
alias vi=nvim
# Misc.
alias serial='sudo dmidecode -s system-serial-number'
alias vsu="rsync --rsh='ssh -p1813' -rtvzP --delete --exclude '.git' . root@vern.cc:/var/www/website --exclude=/blog"
alias wu='git push && ssh aryak@vern.cc '\''cd /home/aryak/public_html && git pull'\'''
alias webcheck="curl https://publapi.p.projectsegfau.lt/users | jq '.users[] | .website' -r | xargs librewolf"
alias curl='curl --proto-default https'
alias exifclear='find ./ -type f -exec exiftool -all= "{}" \; && find ./ -type f -name "*_original" -exec rm -rf "{}" \;'
alias docked='xrandr --output HDMI-A-0 --mode 1920x1080 && xrandr --output eDP --off'
alias laptop='xrandr --output eDP --mode 1920x1080 && xrandr --output HDMI-A-0 --off'
alias wdocked='wlr-randr --output HDMI-A-1 --on && wlr-randr --output eDP-1 --off'
alias wlaptop='wlr-randr --output eDP-1 --on && wlr-randr --output HDMI-A-1 --off'
