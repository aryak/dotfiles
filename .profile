# Basic Vars
export TERMINAL="alacritty"
export BROWSER="librewolf"
export EDITOR="nvim"
export VISUAL="nvim"
# NPM
export NVM_DIR="$XDG_DATA_HOME"/nvm
export NPM_CONFIG_USERCONFIG=$XDG_CONFIG_HOME/npm/npmrc
# XDG
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_RUNTIME_DIR="/run/user/1000"
# GTK
export GTK2_RC_FILES="${XDG_CONFIG_HOME:-$HOME/.config}/gtk-2.0/gtkrc-2.0"
export GTK2_RC_FILES="$XDG_CONFIG_HOME"/gtk-2.0/gtkrc
# Make apps use XDG_BASE_DIR
export LESSHISTFILE="-"
export INPUTRC="${XDG_CONFIG_HOME:-$HOME/.config}/bash/inputrc"
export PASSWORD_STORE_DIR="${XDG_DATA_HOME:-$HOME/.local/share}/password-store"
export TMUX_TMPDIR="$XDG_RUNTIME_DIR"
export CARGO_HOME="${XDG_DATA_HOME:-$HOME/.local/share}/cargo"
export GOPATH="${XDG_DATA_HOME:-$HOME/.local/share}/go"
export GOROOT="/usr/lib/go-1.21"
export HISTFILE="${XDG_DATA_HOME:-$HOME/.local/share}/history"
export RUSTUP_HOME="$XDG_DATA_HOME"/rustup
export RECOLL_CONFDIR="$XDG_CONFIG_HOME/recoll"
# Fix apps
export MOZ_USE_XINPUT2="1"		# Mozilla smooth scrolling/touchpads.
export AWT_TOOLKIT="MToolkit wmname LG3D"	#May have to install wmname
export _JAVA_AWT_WM_NONREPARENTING=1	# Fix for Java applications in WMs
export WLR_NO_HARDWARE_CURSORS=1
export GNUPGHOME="$XDG_DATA_HOME"/gnupg
export _ble_contrib_fzf_base=/usr/bin/fzf

export ENTE_CLI_CONFIG_PATH=/home/arya/.config/ente

# WAYLAND
# Session XDG specification
#export XDG_SESSION_TYPE=wayland
#export XDG_SESSION_DESKTOP=sway
#export XDG_CURRENT_DESKTOP=sway
#export DESKTOP_SESSION=sway
# XKB
export XKB_DEFAULT_LAYOUT=us
export LIBSEAT_BACKEND=logind
# QT
#export QT_WAYLAND_DISABLE_WINDOWDECORATION=1
#export QT_QPA_PLATFORM=wayland-egl
#export MOZ_ENABLE_WAYLAND=1
#export CLUTTER_BACKEND=wayland
#export SDL_VIDEODRIVER=wayland

# IBus
#export GTK_IM_MODULE=ibus
#export XMODIFIERS=@im-ibus
#export QT_IM_MODULE=ibus

# PATH
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi
if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi
if [ -d "$HOME/.local/share/cargo/bin" ]; then
    PATH="$HOME/.local/share/cargo/bin:$PATH"
fi
if [ -d "$HOME/.local/share/go/bin" ]; then
    PATH="$HOME/.local/share/go/bin:$PATH"
fi
if [ -d "/usr/lib/go-1.21/bin" ]; then
    PATH="/usr/lib/go-1.21/bin:${PATH}"
fi
# BASH
. "$HOME/.bashrc"
. /usr/share/bash-completion/*
. "/home/arya/.local/share/cargo/env"
