# If not running interactively don't do anything
[[ $- != *i* ]] && return

# Permanent history with time
export HISTCONTROL=ignoredups:erasedups
export HISTFILESIZE=
export HISTSIZE=
export HISTTIMEFORMAT="[%F %T] "
# Change the file location because certain bash sessions truncate .bash_history file upon close.
# http://superuser.com/questions/575479/bash-history-truncated-to-500-lines-on-each-login
export HISTFILE=~/.bash_history
# Force prompt to write history after every command.
# http://superuser.com/questions/20900/bash-history-loss
PROMPT_COMMAND="history -a; $PROMPT_COMMAND"

# Shell Options
shopt -s autocd # change to named directory
shopt -s cdspell # autocorrects cd misspellings
shopt -s cmdhist # save multi-line commands in history as single line
shopt -s dotglob
shopt -s histappend # do not overwrite history
shopt -s expand_aliases # expand aliases
shopt -s extglob
#ignore upper and lowercase when TAB completion
bind "set completion-ignore-case on"

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if [ "x${BASH_VERSION-}" != x -a "x${PS1-}" != x -a "x${BASH_COMPLETION_VERSINFO-}" = x ]; then
    # Check for recent enough version of bash.
    if [ "${BASH_VERSINFO[0]}" -gt 4 ] ||
        [ "${BASH_VERSINFO[0]}" -eq 4 -a "${BASH_VERSINFO[1]}" -ge 2 ]; then
        [ -r "${XDG_CONFIG_HOME:-$HOME/.config}/bash_completion" ] &&
            . "${XDG_CONFIG_HOME:-$HOME/.config}/bash_completion"
        if shopt -q progcomp && [ -r /usr/share/bash-completion/bash_completion ]; then
            # Source completion code.
            . /usr/share/bash-completion/bash_completion
        fi
    fi
fi

# Random Functions
random() { tr -dc A-Za-z0-9 </dev/urandom | head -c ${1:-10}; }
percentage() { read -p "Total Amount: " total; read -p "x out of ${total}: " number; new=$(echo "scale=2; $number/$total" | bc); ans=$(echo "scale=2; $new*100" | bc); echo $ans; }
ipdom() { curl ipinfo.io/$(dig A ${1} +short | head -n1); }
ipi() { curl ipinfo.io/${1}; }
stopwatch() {
    start=$(date +%s)
    while true; do
        time="$(( $(date +%s) - $start))"
        printf '%s\r' "$(date -u -d "@$time" +%H:%M:%S)"
        sleep 0.1
    done
}
countdown() {
    start="$(( $(date '+%s') + $1))"
    while [ $start -ge $(date +%s) ]; do
        time="$(( $start - $(date +%s) ))"
        printf '%s\r' "$(date -u -d "@$time" +%H:%M:%S)"
        sleep 0.1
    done
}
lscd() { cd ${1} && exa -lah --color=always --group-directories-first --icons; }

export GPG_TTY=$(tty)
export TERM=xterm-256color

eval $(ssh-agent) > /dev/null
. "$HOME/.prompt"
. "$HOME/.bash_aliases"
. "/home/arya/.local/share/cargo/env"

PATH="/home/arya/perl5/bin${PATH:+:${PATH}}"; export PATH;
PERL5LIB="/home/arya/perl5/lib/perl5${PERL5LIB:+:${PERL5LIB}}"; export PERL5LIB;
PERL_LOCAL_LIB_ROOT="/home/arya/perl5${PERL_LOCAL_LIB_ROOT:+:${PERL_LOCAL_LIB_ROOT}}"; export PERL_LOCAL_LIB_ROOT;
PERL_MB_OPT="--install_base \"/home/arya/perl5\""; export PERL_MB_OPT;
PERL_MM_OPT="INSTALL_BASE=/home/arya/perl5"; export PERL_MM_OPT;
